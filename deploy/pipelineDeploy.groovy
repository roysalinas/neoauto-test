#!/usr/bin/env groovy

def call(def pParams) {
  def fnSteps = evaluate readTrusted("deploy/steps.groovy")

  pipeline {
    agent any
    parameters {
      choice(
        name: 'ENVIRONMENT',
        choices:"dev\npre\nprod",
        description: '''DEV: Desplegar en dev
PRE: Desplegar en pre
PROD: Desplegar en prod'''
      )
      choice(
        name: 'EXECUTE',
        choices:"DEPLOY\nROLLBACK\nREGISTRY",
        description: '''DEPLOY: Se realiza deploy del servicio
ROLLBACK: Rollback de la última migración
REGISTRY: Requiere construir o no Registry en ECR'''
      )
    }
    stages {
      stage('Checkout') {
        steps {
          checkout scm
        }
      }

      stage('Tests') {
        steps {
          echo "sh 'make run-test'"
        }
        post {
          always {
            echo "junit '**/build/reports/junit/xml/*.xml'"
          }
        }
      }

      /* #################### Development #################### */

      stage('Development Config') {
        when { expression { return params.ENVIRONMENT == pParams.DEVELOPMENT_ENV } }
        steps {
          script {
            config = fnSteps.configs(pParams.DEVELOPMENT_ENV)
          }
        }
      }
      stage('Development') {
        when { expression { return params.ENVIRONMENT == pParams.DEVELOPMENT_ENV } }
        parallel {
          stage('Deploy') {
            when { expression { return params.EXECUTE == 'DEPLOY' }}
            steps { script { fnSteps.deploy(config) } }
          }
          stage('Rollback') {
            when { expression { return params.EXECUTE == 'ROLLBACK' }}
            steps { script { fnSteps.rollback(config) } }
          }
          stage('ECR') {
            when { expression { return params.EXECUTE == 'REGISTRY' }}
            steps { script { fnSteps.registry(config) } }
          }
        }
      }

      /* #################### Staging #################### */

      stage ('Staging Config') {
        when { expression { return params.ENVIRONMENT == pParams.STAGING_ENV } }
        steps {
          input "Continue deployment to Staging?"
          script {
            config = fnSteps.configs(pParams.STAGING_ENV)
          }
        }
      }

      stage('Staging') {
        when { expression { return params.ENVIRONMENT == pParams.STAGING_ENV } }
        parallel {
          stage('Deploy') {
            when { expression { return params.EXECUTE == 'DEPLOY' }}
            steps { script { fnSteps.deploy(config) } }
          }
          stage('Rollback') {
            when { expression { return params.EXECUTE == 'ROLLBACK' }}
            steps { script { fnSteps.rollback(config) } }
          }
          stage('ECR') {
            when { expression { return params.EXECUTE == 'REGISTRY' }}
            steps { script { fnSteps.registry(config) } }
          }
        }
      }

      /* #################### Production #################### */

      stage ('Production Config') {
        when { expression { return params.ENVIRONMENT == pParams.PRODUCTION_ENV } }
        steps {
          input "Continue deployment to Production?"
          script {
            config = fnSteps.configs(pParams.PRODUCTION_ENV)
          }
        }
      }

      stage('Production') {
        when { expression { return params.ENVIRONMENT == pParams.PRODUCTION_ENV } }
        parallel {
          stage('Deploy') {
            when { expression { return params.EXECUTE == 'DEPLOY' }}
            steps { script { fnSteps.deploy(config) } }
          }
          stage('Rollback') {
            when { expression { return params.EXECUTE == 'ROLLBACK' }}
            steps { script { fnSteps.rollback(config) } }
          }
          stage('ECR') {
            when { expression { return params.EXECUTE == 'REGISTRY' }}
            steps { script { fnSteps.registry(config) } }
          }
        }
      }
    }
  }
}

return this