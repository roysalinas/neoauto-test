#!/usr/bin/env groovy

def call(def pParams) {
  pipeline {
    agent any
    stages {
      stage('Checkout') {
        steps {
          checkout scm
        }
      }

      stage('Tests') {
        steps {
          echo "sh 'make run-test'"
        }
      }
    }
    post {
      always {
        echo "junit '**/build/reports/junit/xml/*.xml'"
      }
    }
  }
}

return this
